import TodoActionTypes from './TodoActionTypes';
import TodoDispatcher from './TodoDispatcher';

let todoId = 1111;

const Actions = {
  addTodo (text) {
    TodoDispatcher.dispatch({
      type: TodoActionTypes.ADD_TODO,
      text,
      id: '' + todoId++
    });
  },
  deleteTodo (id) {
    TodoDispatcher.dispatch({
      type: TodoActionTypes.DELETE_TODO,
      id
    });
  },
  toggleTodo (id) {
    TodoDispatcher.dispatch({
      type: TodoActionTypes.TOGGLE_TODO,
      id
    });
  },
}

export default Actions;

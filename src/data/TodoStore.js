import Immutable from 'immutable';
import {ReduceStore} from 'flux/utils';

import TodoActionTypes from './TodoActionTypes';
import TodoDispatcher from './TodoDispatcher';
import Todo from './Todo';

class TodoStore extends ReduceStore {
  constructor () {
    super (TodoDispatcher);
  }

  getInitialState () {
    return Immutable.OrderedMap();
  }

  reduce (state, action) {
    debugger;
    switch (action.type) {
      case TodoActionTypes.ADD_TODO:
        if (!action.text) {
          return state;
        }
        const id = parseInt(Math.random() * 999999);
        return state.set(id, new Todo({
          id,
          text: action.text,
          complete: false
        }));

      case TodoActionTypes.DELETE_TODO:
        return state.delete(parseInt(action.id));

      case TodoActionTypes.TOGGLE_TODO:
        return state.update(parseInt(action.id),
              todo => todo.set('complete', !todo.complete));

      default:
        return state;
    }
  }
}

export default new TodoStore();

import React, { Component } from 'react';

class TodoItem extends Component {
  constructor () {
    super ();
  }

  /**
   * Handler function for toggle todo
   */

  _onToggleClick (event) {
    const { dispatch } = this.props;
    const id = event.target.dataset.id;
    this.props.onToggleTodo(id);
  }

  /**
   * Handler function for delete todo
   */

  _onDeleteClick (event) {
    const { dispatch } = this.props;
    const id = event.target.dataset.id;
    this.props.onDeleteTodo(id);
  }


  render () {
    const {text, completed, id} = this.props;
    const toggleClass = "glyphicon glyphicon-ok toggle-todo-" + (completed ? "done" : "open");
    const textClass = "todo-text " + (completed ? "done" : "open");
    return (
      <li>
        <span
          className={toggleClass}
          onClick={this._onToggleClick.bind(this)}
          data-id={id}>
        </span>

        <span data-id={id} className={textClass}>{text}</span>

        <span
          data-id={id}
          className="close glyphicon glyphicon-remove-circle"
          onClick={this._onDeleteClick.bind(this)} >
        </span>
      </li>
    )
  }
}

export default TodoItem;

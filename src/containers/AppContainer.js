import { Container } from 'flux/utils';
import React, { Component } from 'react';
import TodoStore from '../data/TodoStore';

import TodoActions from '../data/TodoActions';

import Header from '../views/Header';
import Todos from '../views/Todos';

import AddTodo from './AddTodo';

class AppContainer extends Component {
  static getStores () {
    return [TodoStore];
  }

  static calculateState (prevState) {
    return {
      todos: TodoStore.getState(),

      onDeleteTodo: TodoActions.deleteTodo,
      onToggleTodo: TodoActions.toggleTodo,
      addTodo: TodoActions.addTodo
    }
  }
  render () {
    return (
      <div>
        <Header />
        <AddTodo addTodo={this.state.addTodo} />
        <Todos
          todos={this.state.todos.toArray()}
          onToggleTodo={this.state.onToggleTodo}
          onDeleteTodo={this.state.onDeleteTodo}
          />
      </div>
    )
  }
}

AppContainer = Container.create(AppContainer);

export default AppContainer;

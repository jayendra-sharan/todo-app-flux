import React from 'react';
import {render} from 'react-dom';
import AppContainer from './containers/AppContainer';
import { Provider } from 'react-redux';

import '../styles/index.scss';
import TodoActions from './data/TodoActions';

render(<AppContainer />, document.querySelector("#app"));

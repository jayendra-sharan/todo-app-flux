import React, { Component } from 'react';
import { connect } from 'react-redux';

import TodoItem from '../containers/TodoItem';

class Todos extends Component {
  constructor () {
    super ();
  }

  render () {
    const {todos} = this.props;
    debugger;
    return (
      <ul className="todo-list">
        {todos.map(todo => {
          return (
            <TodoItem
              key={todo.get('id')}
              text={todo.get('text')}
              completed={todo.get('complete')}
              id={todo.id}
              onDeleteTodo={this.props.onDeleteTodo}
              onToggleTodo={this.props.onToggleTodo}
              />
          )

        })}
      </ul>
    )
  }
}

export default Todos;
